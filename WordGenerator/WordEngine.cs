﻿using System.IO;
using System.Reflection;

namespace WordGenerator
{
    public class WordEngine
    {
        private static Generator generator;
        public static void Initialize()
        {
            if (generator == null)
            {
                var root = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                var dictionaryPath = Path.Combine(root, "words.db");
                Trie trie = new Trie(new StringStream(dictionaryPath));
                generator = new Generator(trie);
            }
        }

        public static Generator Generator
        {
            get
            {
                if (generator != null)
                    Initialize();
                return generator;
            }
        }

    }
}