﻿using System.Collections.Generic;

namespace WordGenerator
{
    
    public class Generator
    {
        #region Declarations

        readonly Trie validWords;

        List<string> words = new List<string>();

        #endregion

        #region Initialization

        public Generator(Trie validWords)
        {
            this.validWords = validWords;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Find all possible words from this set of letters
        /// </summary>
        /// <param name="letters">must be all lower case</param>
        /// <returns></returns>
        public ICollection<string> AllPossibleWordsContaining(IEnumerable<char> letters, char hasLetter)
        {
            var node = validWords.Root;
            int[] availableLetters = new int[26];
            foreach (var letter in letters)
            {
                int location = letter - TrieNode.ASCIIA;
                availableLetters[location]++;
            }

            words = new List<string>();

            VisitChoicesContaining(validWords.Root, availableLetters, string.Empty, hasLetter, false);

            return words;
        }

        private void VisitChoicesContaining(TrieNode node, int[] lettersAvailable, string wordSoFar, char containingLetter, bool hasLetter)
        {
            if (node.IsEnd && hasLetter)
                words.Add(wordSoFar);

            for (int i = 0; i < 26; i++)
            {
                var child = node.Nodes[i];

                if (child != null && lettersAvailable[i] > 0)
                {
                    lettersAvailable[i]--;

                    char newLetter = (char)(i + TrieNode.ASCIIA);
                    var word = wordSoFar + newLetter;

                    VisitChoicesContaining(child, lettersAvailable, word, containingLetter, hasLetter || newLetter == containingLetter);

                    lettersAvailable[i]++;
                }
            }
        }


        /// <summary>
        /// Find all possible words from this set of letters
        /// </summary>
        /// <param name="letters">must be all lower case</param>
        /// <returns></returns>
        public IEnumerable<string> AllPossibleWords(IEnumerable<char> letters)
        {
            var node = validWords.Root;
            int[] availableLetters = new int[26];
            foreach (var letter in letters)
            {
                int location = letter - TrieNode.ASCIIA;
                availableLetters[location]++;
            }

            words = new List<string>();

            VisitChoices(validWords.Root, availableLetters, string.Empty);

            return words;
        }

        private void VisitChoices(TrieNode node, int[] lettersAvailable, string wordSoFar)
        {
            if (node.IsEnd)
                words.Add(wordSoFar);

            for (int i = 0; i < 26; i++)
            {
                var child = node.Nodes[i];

                if (child != null && lettersAvailable[i] > 0)
                {
                    lettersAvailable[i]--;

                    char newLetter = (char)(i + TrieNode.ASCIIA);
                    var word = wordSoFar + newLetter;

                    VisitChoices(child, lettersAvailable, word);

                    lettersAvailable[i]++;
                }
            }
        }

        #endregion
    }
}
