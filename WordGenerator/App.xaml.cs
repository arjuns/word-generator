﻿using System;
using System.Diagnostics;
using System.Windows;

namespace WordGenerator
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            string procName = Process.GetCurrentProcess().ProcessName;
            // get the list of all processes by that name

            Process[] processes = Process.GetProcessesByName(procName);

            if (processes.Length > 1)
            {
                MessageBox.Show("Free Word Generator is already running!","Free Word Generator",MessageBoxButton.OK,MessageBoxImage.Information);
                Environment.Exit(0);
            }
            
        }
    }
   
}
