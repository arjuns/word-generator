﻿using System.Diagnostics;
using System.Windows;

namespace WordGenerator
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : Window
    {
        public AboutWindow()
        {
            InitializeComponent();
            this.Loaded += WindowLoaded;

        }
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            this.MouseLeftButtonDown += (a, b) => DragMove();
            this.CloseButton.Click += (a, b) => Close();
        }

        private void OpenEmailClient(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start("mailto:support@softdevresource.com");
            }
            catch
            {

            }
        }
        private void OpenWebSite(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start("http://www.softdevresource.com");
            }
            catch
            {

            }
        }
    }
}
