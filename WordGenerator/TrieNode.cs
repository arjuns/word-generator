﻿using System;

namespace WordGenerator
{
    public class TrieNode
    {
        public TrieNode[] Nodes;
        public bool IsEnd = false;
        public const int ASCIIA = 97;
        public TrieNode()
        {
            Nodes = new TrieNode[26];
        }
        public bool Contains(char c)
        {
            int n = Convert.ToByte(c) - ASCIIA;

            if (n < 26)
                return (Nodes[n] != null);
            else
                return false;
        }
        public TrieNode GetChild(char c)
        {
            int n = Convert.ToByte(c) - ASCIIA;
            return Nodes[n];
        }
    }
}