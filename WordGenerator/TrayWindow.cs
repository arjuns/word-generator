﻿using System;
using System.ComponentModel;
using System.Windows;
using WordGenerator.NotifyIcons;

namespace WordGenerator
{
    public class TrayWindow : Window
    {
        private WindowState winState;
        private bool goneToTray;
        private TaskbarIcon notifyIconHost;

        protected void TrayActivated(object sender, RoutedEventArgs e)
        {
            Show();
            Topmost = true;
            WindowState = winState;
            Topmost = false;

        }
        public TaskbarIcon NotifyIconHost
        {
            get { return notifyIconHost; }
            set
            {
                notifyIconHost = value;
                this.notifyIconHost.TrayMouseDoubleClick += (a, b) =>
                {
                    this.notifyIconHost.ToolTipText = "Free Word Generator";
                    TrayActivated(null, null);
                };

            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            ShowInfo();
            e.Cancel = true;
            Hide();
            base.OnClosing(e);
        }


        protected override void OnStateChanged(EventArgs e)
        {

            if (WindowState == WindowState.Minimized)
            {
                ShowInfo();
                this.ShowInTaskbar = false;
                Hide();
            }
            else
            {
                winState = WindowState;
                this.ShowInTaskbar = true;
            }
            base.OnStateChanged(e);
        }

        private void ShowInfo()
        {
            if (!goneToTray)
            {
                goneToTray = true;
                if (this.NotifyIconHost != null)
                    this.NotifyIconHost.ShowBalloonTip("Free Word Generator", "Hi, I am here", BalloonIcon.Info);
            }
        }
    }
}