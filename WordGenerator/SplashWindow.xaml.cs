﻿using System;
using System.Threading;
using System.Windows;

namespace WordGenerator
{
    /// <summary>
    /// Interaction logic for SplashWindow.xaml
    /// </summary>
    public partial class SplashWindow : Window
    {
        public SplashWindow()
        {
            InitializeComponent();
            this.Loaded += (a, b) => InitializeGenerator();
        }

        private void InitializeGenerator()
        {
            WordEngine.Initialize();

            AutoResetEvent evt = new AutoResetEvent(false);
            OnUIThread(() =>
            {
                this.InfoTextBlock.Text = "Please wait while program initializes....";
            });
            ThreadPool.QueueUserWorkItem((a) =>
            {
                try
                {

                    WordEngine.Initialize();
                    evt.Set();
                }
                catch (Exception ex)
                {
                    OnUIThread(() =>
                    {
                        var msg = String.Format("Error Occured! while initializing dictionary \r\n {0} \r\n Program will stop now", ex.Message);
                        MessageBox.Show(this, msg, "Free Word Generator", MessageBoxButton.OK, MessageBoxImage.Error);
                        Environment.Exit(0);
                        evt.Set();
                    });


                }
                OnUIThread(() =>
                {
                    this.InfoTextBlock.Text = "Dictionary initialize complete..";
                });
                Thread.Sleep(1000);
                evt.WaitOne();
                OnUIThread(Close);

            });
        }

        private void OnUIThread(Action act)
        {
            Dispatcher.BeginInvoke(act);
        }
    }


}
