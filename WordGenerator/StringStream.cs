﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace WordGenerator
{
    public class StringStream : IEnumerable<string>
    {
        private readonly string path;
        private readonly FileStream fs;
        public StringStream(string path)
        {
            this.path = path;
            fs = new FileStream(path, FileMode.Open);
        }

        public IEnumerator<string> GetEnumerator()
        {
            StreamReader reader = new StreamReader(fs);
            string line = null;
            while ((line = reader.ReadLine()) != null)
                yield return line.ToLower();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}