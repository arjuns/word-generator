﻿using System;
using System.Collections.Generic;

namespace WordGenerator
{
    public class Trie
    {

        private readonly TrieNode root = new TrieNode();
        public Trie(IEnumerable<string> words)
        {

            foreach (var word in words)
                Insert(word);
        }

        public Trie()
        { }
        public TrieNode Insert(string s)
        {
            char[] charArray = s.ToLower().ToCharArray();
            TrieNode node = root;
            foreach (char c in charArray)
            {
                //added by me
                if (!Char.IsLetter(c) || (int)c > 122) continue; //122 =='z'

                node = Insert(c, node);
            }
            node.IsEnd = true;
            return root;
        }
        private TrieNode Insert(char c, TrieNode node)
        {
            if (node.Contains(c))
                return node.GetChild(c);
            else
            {
                int n = Convert.ToByte(c) - TrieNode.ASCIIA;

                TrieNode t = new TrieNode();
                node.Nodes[n] = t;
                return t;
            }
        }
        public bool Contains(string s)
        {
            char[] charArray = s.ToLower().ToCharArray();
            TrieNode node = root;
            bool contains = true;
            foreach (char c in charArray)
            {
                node = Contains(c, node);
                if (node == null)
                {
                    contains = false;
                    break;
                }
            }
            if ((node == null) || (!node.IsEnd))
                contains = false;
            return contains;
        }
        private TrieNode Contains(char c, TrieNode node)
        {
            if (node.Contains(c))
            {
                return node.GetChild(c);
            }
            else
            {
                return null;
            }
        }
        public TrieNode Root
        {
            get { return root; }
        }
    }
}