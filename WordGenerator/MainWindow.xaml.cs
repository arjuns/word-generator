﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Input;

namespace WordGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : TrayWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += WindowLoaded;
            this.NotifyIconHost = this.TrayIcon;
            var win = new SplashWindow();
            win.ShowDialog();
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {

            this.MouseLeftButtonDown += (a, b) => DragMove();
            this.CloseButton.Click += (a, b) => Close();
            this.InputTextBox.Focus();

        }


        private string prefix;
        private string suffix;
        private string lengthInput;


        private void GenerateWordsClicked(object sender, RoutedEventArgs e)
        {
            lengthInput = this.TextBoxLength.Text.Trim();
            prefix = this.TextBoxPrefix.Text.Trim().ToLowerInvariant();
            suffix = this.TextBoxSuffix.Text.Trim().ToLowerInvariant();
            if (!ValidatateInput())
            {
                this.TextBlockWordCount.Text = "Invalid input. All Input character should be a letter. ";
                return;
            }
            ResetResult();
            var var = this.InputTextBox.Text.Replace(" ", "").ToLowerInvariant();
            ThreadPool.QueueUserWorkItem((a) =>
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    this.TextBlockWordCount.Text = "Generating all possible words ...";
                }));

                var allPossibleWords = WordEngine.Generator.AllPossibleWords(var)
                    .Where(GetFilter)
                    .ToList();

                Dispatcher.BeginInvoke(new Action(() =>
                {
                    WordsListBox.ItemsSource = allPossibleWords;
                    this.TextBlockWordCount.Text = String.Format("{0} Words Generated", allPossibleWords.Count);
                }));

            });

        }

        private bool GetFilter(string item)
        {
            if (String.IsNullOrEmpty(item))
                return false;
            bool result = true;

            if (!String.IsNullOrEmpty(prefix))
            {
                if (!item.StartsWith(prefix))
                {
                    result = false;
                }
            }

            if (!String.IsNullOrEmpty(suffix))
            {
                if (!item.EndsWith(suffix))
                {
                    result = false;
                }
            }


            if (lengthInput.StartsWith("+"))
            {
                int wordLength;
                if (Int32.TryParse(lengthInput.Replace("+", ""), out wordLength))
                {
                    return result & item.Length > wordLength;
                }

            }

            else if (lengthInput.StartsWith("-"))
            {
                int wordLength;
                if (Int32.TryParse(lengthInput.Replace("-", ""), out wordLength))
                {
                    return result & item.Length < wordLength;
                }

            }

            else
            {
                int wordLength;
                if (Int32.TryParse(lengthInput, out wordLength))
                {
                    result &= item.Length == wordLength;
                }

            }

            return result;
        }

        private bool ValidatateInput()
        {
            var input = this.InputTextBox.Text.Trim();
            if (input.All(Char.IsLetter))
            {
                return true;
            }
            return false;
        }

        public void ResetResult()
        {
            this.TextBlockWordCount.Text = "";
            WordsListBox.ItemsSource = null;
        }

        private void ClearPrefixClicked(object sender, RoutedEventArgs e)
        {
            this.TextBoxPrefix.Text = "";
        }
        private void ClearSuffixClicked(object sender, RoutedEventArgs e)
        {
            this.TextBoxSuffix.Text = "";
        }

        private void InputTextBoxKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                GenerateWordsClicked(null, null);
            }
        }

        private void SaveToClibboardClicked(object sender, RoutedEventArgs e)
        {
            List<string> result = this.WordsListBox.ItemsSource as List<string>;
            if (result != null && result.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (string item in result)
                {
                    sb.AppendLine(item);
                }
                Clipboard.SetText(sb.ToString());
                var msg = String.Format("{0} words saved to clipboard.", result.Count);
                MessageBox.Show(msg, "Free Word Generator", icon: MessageBoxImage.Information,
                    button: MessageBoxButton.OK);
            }
        }


        private AboutWindow aboutWindow;
        private void AboutClicked(object sender, RoutedEventArgs e)
        {
            if (aboutWindow == null)
            {
                aboutWindow = new AboutWindow() {Owner = this};
                aboutWindow.Closed += (a, b) => aboutWindow = null;
                aboutWindow.Show();
            }
            else
            {
                aboutWindow.Activate();
            }
           
        }

        private void ClearLengthClicked(object sender, RoutedEventArgs e)
        {
            this.TextBoxLength.Text = "";
        }

        private void TrayMenuOpenClicked(object sender, RoutedEventArgs e)
        {
            this.TrayActivated(sender, e);
        }

        private void ExitMenuClicked(object sender, RoutedEventArgs e)
        {
            this.TrayIcon.Dispose();
            Environment.Exit(0);
        }
    }
}
